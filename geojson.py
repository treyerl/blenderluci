__author__ = 'treyerl'


import bpy
import bmesh
from mathutils import Vector


def pointlist2bm(bm, li):
    return [bm.verts.new(c) for c in li]


def pointlist2curve(c, li):
    spline = c.splines.new("POLY")
    spline.use_cyclic_u = True
    spline.points.add(len(li) - 2)
    for i in range(len(li) - 1):
        spline.points[i].co = Vector(li[i]).to_4d()

def closedsplinepoints(spline):
    for v in spline.points:
        yield v
    if len(spline.points) > 0:
        yield spline.points[0]

def faceverts(face):
    for v in face.verts:
        yield v
    if len(face.verts) > 0:
        yield face.verts[0]

def face2pointlist(face, world_mat):
    return [[list(world_mat * v.co) for v in faceverts(face)]]


def faces2pointlist(faces, world_mat):
    return [face2pointlist(face, world_mat) for face in faces]


def edges2multilinestring(edges, world_mat):
    return [[list(world_mat * edge.verts[0].co), list(world_mat * edge.verts[1].co)] for edge in edges]


def edges2linestring(edges, world_mat):
    linestring = set([])
    linestring.add((world_mat * edges[0].verts[0].co).to_tuple())
    linestring.add((world_mat * edges[0].verts[0].co).to_tuple())
    for i in range(1, len(edges)):
        ev = edges[i].verts
        if ev[1].co.to_tuple() in linestring:
            v = world_mat * ev[0].co
        else: 
            v = world_mat * ev[1].co
        linestring.add(v.to_tuple())
    return list(linestring)


def geojson_2_bl_geometry(gjg, name):
    bm = c = None
    t = gjg['type']

    if t == "Point":
        bm = bmesh.new()
        bm.verts.new(gjg['coordinates'])

    if t == "MultiPoint":
        bm = bmesh.new()
        pointlist2bm(bm, gjg['coordinates'])

    if t == "LineString":
        bm = bmesh.new()
        verts = pointlist2bm(bm, gjg['coordinates'])
        # print("LineString ", len(verts))
        for i in range(1, len(verts)):
            bm.edges.new(verts[i-1:i+1])

    if t == "MultiLineString":
        bm = bmesh.new()
        for line in gjg['coordinates']:
            verts = pointlist2bm(bm, line)
            # print("MultiLineString ", len(verts))
            for i in range(1, len(verts)):
                bm.edges.new(verts[i-1:i+1])

    if t == "Polygon":
        if len(gjg['coordinates']) == 1:
            bm = bmesh.new()
            bm.faces.new((bm.verts.new(point) for point in gjg['coordinates'][0]))
        else:
            c = bpy.data.curves.new(name, "CURVE")
            c['outers'] = [0]
            for ring in gjg['coordinates']:
                pointlist2curve(c, ring)

    if t == "MultiPolygon":
        coords = gjg['coordinates']
        if all((len(poly) == 1 for poly in coords)):
            bm = bmesh.new()
            for poly in coords:
                bm.faces.new((bm.verts.new(point) for point in poly[0]))
        else:
            c = bpy.data.curves.new(name, "CURVE")
            outers = []
            i = 0
            for poly in gjg['coordinates']:
                outers.append(i)
                i += len(poly)
                for ring in poly:
                    pointlist2curve(c, ring)
            c['outers'] = outers

    if bm is not None:
        m = bpy.data.meshes.new(name)
        bm.to_mesh(m)
        return m
    if c is not None:
        return c


def mesh2geojson(obj):
    bm = bmesh.new()
    bm.from_mesh(obj.data)
    world_mat = obj.matrix_world
    result = []
    if len(bm.faces) > 0:
        bm.faces.ensure_lookup_table()
        if len(bm.faces) == 1:
            result.append({'type': 'Polygon', 'coordinates': face2pointlist(bm.faces[0], world_mat)})
        else:
            result.append({'type': 'MultiPolygon', 'coordinates': faces2pointlist(bm.faces, world_mat)})
    elif len(bm.edges) > 0:
        bm.edges.ensure_lookup_table()
        if all((len(v.link_edges) == 2 for v in bm.verts)):
            result.append({'type': 'LineString', 'coordinates': edges2linestring(bm.edges, world_mat)})
        else:
            result.append({'type': 'MultiLineString', 'coordinates': edges2multilinestring(bm.edges, world_mat)})
    elif len(bm.verts) > 0:
        bm.verts.ensure_lookup_table()
        if len(bm.verts) == 1:
            result.append({'type': 'Point', 'coordinates': list(world_mat * bm.verts[0].co)})
        else:
            result.append({'type': 'MultiPoint', 'coordinates': [list(world_mat * v.co) for v in bm.verts]})
    return result


def curve2geojson(obj, scn): # scn = scene
    curve = obj.data

    if all(((len(s.bezier_points) == 0) for s in curve.splines)):
        if 'outers' in curve:
            coords = []
            outers = curve['outers']
            if len(outers) == 1:
                polygon = {'type': 'Polygon'}
                for spline in curve.splines:
                    coords.append([list(obj.matrix_world * p.co.to_3d()) for p in closedsplinepoints(spline)])
            else:
                for limit_index in range(1, len(outers)):
                    lower = outers[limit_index - 1]
                    upper = outers[limit_index]
                    poly = []
                    for i in range(lower, upper):
                        spline = curve.splines[i]
                        poly.append([list(obj.matrix_world * p.co.to_3d()) for p in closedsplinepoints(spline)])
                    coords.append(poly)
                polygon = {'type': 'MultiPolygon', 'coordinates': coords}
            polygon['coordinates'] = coords
            return [polygon]
        else:
            result = []
            lines = []
            polys = []
            for spline in curve.splines:
                if spline.use_cyclic_u:
                    polys.append([[list(p.co) for p in closedsplinepoints(spline)]])
                else:
                    lines.append([list(p.co) for p in spline.points])
            if len(polys) > 0:
                if len(polys) == 1:
                    polygon = {'type': 'Polygon'}
                    polys = polys[0]
                else:
                    polygon = {'type': 'MultiPolygon'}
                polygon['coordinates'] = polys
                result.append(polygon)
            if len(lines) > 0:
                if len(lines) == 1:
                    linestring = {'type': 'LineString'}
                    linestring['coordinates'] = lines[0]
                else:
                    linestring = {'type': 'MultiLineString'}
                    linestring['coordinates'] = lines
                
                result.append(linestring)
            return result
    else:
        m = obj.to_mesh(scn, apply_modifiers=True, settings='PREVIEW')
        return mesh2geojson(bpy.data.objects.new(obj.name, m))

def feature(geometry, **kwargs):
    feature = {'type': 'Feature'}
    if geometry is not None:
        feature['geometry'] = geometry
    # if len(kwargs) > 0:
    properties = {}
    for k, v in kwargs.items():
        properties[k] = v
    feature['properties'] = properties
    return feature


def object2features(obj, scn, with_id=True):
    features = []
    kwargs = {}
    if with_id:
        kwargs['geomID'] = obj['geomID']
    if len(obj.users_group) > 0:
        kwargs['layer'] = obj.users_group[0].name
    geometries = None
    if obj.type == "CURVE":
        geometries = curve2geojson(obj, scn)
    if obj.type == "MESH":
        geometries = mesh2geojson(obj)

    if geometries is not None:
        features.append(feature(geometries[0], **kwargs))
        if with_id:
            del kwargs['geomID']
        for i in range(1,len(geometries)):
            geometry = geometries[i]
            features.append(feature(geometry, **kwargs))
    elif len(kwargs) > 0:
        features.append(feature(None, **kwargs))
    return features

def features2FeatureCollection(features):
    return {'type': 'FeatureCollection', 'features': features}