from .lucipy import LcClient

luci_client = LcClient(generatesCallIDs=True)
last_selected_ids = list()
mouse_pressed = False
camera = dict()
camera_updated = False
camera_subscribed = False
