import bpy
import types
import mathutils
import traceback
from .globals import luci_client, mouse_pressed
from .geojson import object2features, features2FeatureCollection
from bpy.props import BoolProperty, StringProperty, IntProperty, CollectionProperty, EnumProperty
from .lucipy import Message
from .handlers import import_scenario, scene_update_post, last_selected_clear, stdResponseHandler

bl_info = {
    "name": "Luci Connector",
    "author": "Alessandro Forino, Lukas Treyer",
    "version": (0, 1, 0),
    "blender": (2, 7, 1),
    "location": "Tools Shelf & Node Editor",
    "description": "Connect and Import from / Export to Luci and Register to its events",
    "wiki_url": "https://bitbucket.org/treyerl/blenderluci",
    "tracker_url": "https://bitbucket.org/treyerl/blenderluci",
    "category": "System",
    }


def diff(first, second):
    second = set(second)
    return [item for item in first if item not in second]

scenarios = dict()
timeout = 60

class LuciScenarios(bpy.types.PropertyGroup):
    ScID = IntProperty()
    #name = StringProperty()


class CheckConnection:
    def sendAndReceive(self, message):
        try:
            response, duration = luci_client.sendAndReceive(message, timeout)
            h = response.getHeader()
            if "error" in h:
                # self.report({'ERROR'}, h['error'])
                raise Exception(h['error'])
            else:
                return response, duration

        except ConnectionError as e:
            self.report({'ERROR'}, str(e))

class ConnectOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.connect_operator"
    bl_label = "Connect"
    
    def execute(self, context):
        try:
            luci_client.setStdResponseHandler(stdResponseHandler)
            host, port = context.scene.luci_server.split(':')
            luci_client.connect(host, port)
            context.scene.connected = True
        except ConnectionError as e:
            self.report({'ERROR'}, str(e))
            return {'FINISHED'}

        bpy.ops.luci.shutdown_detector("EXEC_DEFAULT")
        return bpy.ops.luci.load_operator("EXEC_DEFAULT")

class LoadOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.load_operator"
    bl_label = "Reload List"

    def execute(self, context):
        message, _ = self.sendAndReceive(Message({'run': 'scenario.GetList'}))
        news = message.getHeader()['result']['scenarios']
        olds = context.scene.luci_scenarios
        lc = {s['ScID']: i for i, s in enumerate(news)}
        lb = {s['ScID']: i for i, s in enumerate(olds)}
        toAdd = sorted([lc[i] for i in (set(lc.keys()) - set(lb.keys()))])
        toDel = [lb[i] for i in (set(lb.keys()) - set(lc.keys()))]

        for i in toAdd:
            scenario_item = context.scene.luci_scenarios.add()
            scenario_item.name = news[i]['name']
            scenario_item.ScID = news[i]['ScID']

        for i in toDel:
            olds.remove(i)

        return {'FINISHED'}

    
class DisconnectOperator(bpy.types.Operator):
    bl_idname = "luci.disconnect_operator"
    bl_label = "Disonnect"

    def execute(self, context):
        luci_client.disconnect()
        context.scene.connected = False
        return {'FINISHED'}


class ClearSceneOperator(bpy.types.Operator):
    bl_idname = "luci.clear_scene_operator"
    bl_label = "Clear Scene"

    def execute(self, context):
        bpy.app.handlers.scene_update_post.remove(scene_update_post)
        for object in context.scene.objects:
            object.select = True

        bpy.ops.object.delete()
        last_selected_clear()
        self.report({'INFO'}, "scene cleared")
        bpy.app.handlers.scene_update_post.append(scene_update_post)
        return {'FINISHED'}


class ImportOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.import_operator"
    bl_label = "Import"
    bl_options = {"REGISTER", "UNDO"} 

    def execute(self, context):
        scenarios = context.scene.luci_scenarios
        idx = context.scene.luci_scenarios_idx
        ScID = scenarios[idx]['ScID']

        request = Message({'run': 'scenario.geojson.Get', 'ScID': ScID})
        message, duration = self.sendAndReceive(request)
        print("Scenario received (%f seconds)" % duration)
        self.report({'INFO'}, "Scenario received (%f seconds)" % duration)
        import_scenario(message.getHeader(), context.scene)
        bpy.context.space_data.clip_end = 100000
        luci_client.send(Message({'run': 'scenario.SubscribeTo',
                                  'ScIDs': [ScID], 'format': 'geojson'}),
                         stdResponseHandler())

        return {'FINISHED'}


class RemoveOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.remove_operator"
    bl_label = "Remove"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        bpy.app.handlers.scene_update_post.remove(scene_update_post)
        scenarios = context.scene.luci_scenarios
        idx = context.scene.luci_scenarios_idx
        ScID = scenarios[idx]['ScID']
        for object in context.scene.objects:
            splitted = str(object.name).split('-')
            try:
                sceneID = int(splitted[0])
                if sceneID == ScID:
                    object.select = True
                else:
                    object.select = False
            except ValueError:
                print("object name not a number")
        bpy.ops.object.delete()
        last_selected_clear()
        luci_client.send(Message({'run': 'scenario.UnsubscribeFrom', 'ScIDs': [ScID]}), stdResponseHandler())
        print("scenario deleted")
        bpy.app.handlers.scene_update_post.append(scene_update_post)
        return {'FINISHED'}


class UploadOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.upload_operator"
    bl_label = "Upload"

    def execute(self, context):
        #TODO: if no scenario is selected, create one first
        scid = context.scene.luci_scenarios[context.scene.luci_scenarios_idx]['ScID']
        features = []
        newObjects = []
        for obj in context.selected_objects:
            features.extend(object2features(obj, context.scene, with_id=False))
            if 'geomID' not in obj:
                newObjects.append(obj)
        g = features2FeatureCollection(features)

        request = {"run": "scenario.geojson.Update", "ScID": scid,
                   "geometry_input": {"format": "geojson", "geometry": g}}
        m = Message(request)

        m, _ = self.sendAndReceive(m)
        r = m.getHeader()['result']
        newIDs = r['newIDs']
        for i, obj in enumerate(newObjects):
            obj['geomID'] = newIDs[i]
            obj['timestamp'] = r['lastmodified']
        return {'FINISHED'}

class CreateOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.create_operator"
    bl_label = "Create Scenario"

    newScenarioName = StringProperty(
        name="New Scenario Name",
        description="name for a scenario to be created",
        maxlen=32,
        default="ScenarioX"
    )

    def execute(self, context):
        request = Message({'run': 'scenario.Create', 'name': ""+self.newScenarioName})
        m, _ = self.sendAndReceive(request)
        h = m.getHeader()
        list_item = context.scene.luci_scenarios.add()
        list_item['name'] = h['result']['name']
        list_item['ScID'] = h['result']['ScID']
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


class ErrorOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.error_operator"
    bl_label = "Test Error"

    def execute(self, context):
        request = Message({'run': 'test.Error'})
        self.sendAndReceive(request)
        return {'FINISHED'}


class ShutDownDetector(bpy.types.Operator):
    bl_idname = "luci.shutdown_detector"
    bl_label = "shtdwn"

    def modal(self, context, event):
        return {'PASS_THROUGH'}

    def execute(self, context):
        wm = context.window_manager
        wm.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        print("cancel")
        if len(traceback.format_stack()) == 1:
            luci_client.disconnect()


class ConnectionPanel(bpy.types.Panel):
    bl_label = "Connection"
    bl_idname = "3D_View_TS_luci"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "Luci"


    def draw(self, context):
        # server = context.scene.luci_server.add(
        #     name="Server IP: Port No",
        #     description = "IP address and port number of Luci server (default=localhost)",
        #     maxlen = 128,
        #     default = "localhost:7654")
        # context.scene.luci_server = "localhost:7654"
        layout = self.layout
        row = layout.row()
        row.prop(context.scene, "luci_server", icon="WORLD", text="")
        row.enabled = not bpy.context.scene.connected
        row = layout.row()
        c = row.column()
        c.operator("luci.connect_operator", )
        c.enabled = not bpy.context.scene.connected
        c = row.column()
        c.operator("luci.disconnect_operator")
        c.enabled = bpy.context.scene.connected
        row = layout.row()
        row.prop(context.scene, "auto_sync")
    
class ScenarioPanel(bpy.types.Panel):
    bl_label = "Scenarios"
    bl_idname = "ScenarioPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "Luci"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        row = layout.row()
        row.operator("luci.clear_scene_operator", icon="X")
        layout.template_list("UI_UL_list", "luci_list", scene, "luci_scenarios", scene, "luci_scenarios_idx")

        row = layout.row()
        row.operator("luci.load_operator", icon="FILE_REFRESH")
        row = layout.row()
        row.operator("luci.import_operator", icon="LIBRARY_DATA_DIRECT")
        row = layout.row()
        row.operator("luci.remove_operator", icon="FILE")
        row = layout.row()
        row.operator("luci.upload_operator", icon="TRIA_UP")
        row = layout.row()
        row.operator("luci.create_operator", icon="PLUS")
        row.enabled = scene.connected


class AddCameraOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.camera.new"

    def execute(self, context):
        print("TODO: add camera")


class DeleteCameraOperator(bpy.types.Operator, CheckConnection):
    bl_idname = "luci.camera.delete"

    def execute(self, context):
        print("TODO: delete camera")


class CameraPanel(bpy.types.Panel):
    bl_label = "Cameras"
    bl_idname = "CameraPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "Luci"

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        row = layout.row()
        row.prop(context.scene, "cameraID")
        row = layout.row()
        row.prop(scene, "cameraControl", expand=True)
        # row = layout.row()
        # row.operator("luci.error_operator")


class InfoPanel(bpy.types.Panel):
    bl_label = "Luci Info"
    bl_idname = "LuciInfoButtons"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Luci"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        luciUrl = "http://" + context.scene.luci_server.split(":")[0] + ":8080"
        lucipage = row.operator("wm.url_open", text="Luci Start Page", icon="WORLD")
        lucipage.url = luciUrl
        row = layout.row()
        luciApi = row.operator("wm.url_open", text="Luci API", icon="WORLD")
        luciApi.url = luciUrl + "/api.html"
        row = layout.row()
        bitbucket = row.operator("wm.url_open", text="Bitbucket", icon="WORLD")
        bitbucket.url = "https://bitbucket.org/treyerl/luci2"

def register():
    bpy.utils.register_class(LuciScenarios)
    bpy.utils.register_class(ConnectOperator)
    bpy.utils.register_class(DisconnectOperator)
    bpy.utils.register_class(ClearSceneOperator)
    bpy.utils.register_class(ImportOperator)
    bpy.utils.register_class(RemoveOperator)
    bpy.utils.register_class(CreateOperator)
    bpy.utils.register_class(LoadOperator)
    bpy.utils.register_class(UploadOperator)
    bpy.utils.register_class(ErrorOperator)
    bpy.utils.register_class(ShutDownDetector)

    bpy.utils.register_class(ConnectionPanel)
    bpy.utils.register_class(ScenarioPanel)
    bpy.utils.register_class(CameraPanel)
    bpy.utils.register_class(InfoPanel)

    # bpy.utils.register_class(PrintViewOperator)

    bpy.types.Scene.luci_scenarios = CollectionProperty(type=LuciScenarios)
    bpy.types.Scene.luci_scenarios_idx = IntProperty()
    bpy.types.Scene.luci_server = StringProperty(    
        name="Server",
        description="IP address and port number of Luci server (default=localhost)",
        maxlen=128,
        default="localhost:7654")
    bpy.types.Scene.auto_sync = BoolProperty(
        name="Auto Sync",
        description="Enable or Disable automatic Synchronization",
        default=True)
    bpy.types.Scene.connected = BoolProperty(
        name="Connected",
        description="Boolean to represent if connection is up",
        default=False)
    bpy.types.Scene.cameraID = IntProperty(
        name="CameraID",
        description="CameraID of the camera to follow",
        default=0)
    bpy.types.Scene.cameraControl = EnumProperty(
        items=(('PUBLISH', "Publish", "Publish camera movement"),
               ('LISTEN', "Listen", "Copy external camera movement"),
               ),
        name="cameraControl",
        description="Set the camera behaviour",
    )
    #bpy.types.Scene.cameraIDs = EnumProperty() 
    #bpy.app.handlers.scene_update_pre.append(BL_notify)
    bpy.app.handlers.scene_update_post.append(scene_update_post)


def unregister():
    bpy.utils.unregister_class(LuciScenarios)
    bpy.utils.unregister_class(ConnectOperator)
    bpy.utils.unregister_class(DisconnectOperator)
    bpy.utils.unregister_class(ClearSceneOperator)
    bpy.utils.unregister_class(ImportOperator)
    bpy.utils.unregister_class(RemoveOperator)
    bpy.utils.unregister_class(UploadOperator)
    bpy.utils.unregister_class(LoadOperator)
    bpy.utils.unregister_class(CreateOperator)
    bpy.utils.unregister_class(ErrorOperator)
    bpy.utils.unregister_class(ShutDownDetector)

    bpy.utils.unregister_class(ConnectionPanel)
    bpy.utils.unregister_class(ScenarioPanel)
    bpy.utils.unregister_class(CameraPanel)
    bpy.utils.unregister_class(InfoPanel)

    #bpy.app.handlers.scene_update_pre.remove(BL_notify)
    bpy.app.handlers.scene_update_post.remove(scene_update_post)

if __name__ == "__main__":
    register()


