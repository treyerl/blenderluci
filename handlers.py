import bpy
from .globals import luci_client, camera, camera_updated, camera_subscribed
import mathutils
import time
import math
import copy
import abc
from .geojson import mesh2geojson, curve2geojson, feature, geojson_2_bl_geometry, object2features, features2FeatureCollection
from .lucipy import Message, ResponseHandler
from bpy.app.handlers import persistent
from bpy.props import StringProperty

# import pygame
# print(pygame.mouse.get_pressed())

DEBUG = True
opcount = None
obcount = None
scene_name = None

def collect(j):
    t = j['type']
    if t == "FeatureCollection":
        return j['features']
    if t == "GeometryCollection":
        return j['geometries']
    else:
        return [j]

def replace_objects(json, scn):
    import_scenario(json, scn)

def import_scenario(json, scn):
    #print(json)
    update = json['result']
    # FIXME don't add lastmodified and ScID properties to scene but create one group per scenario
    # and add the properties to the group. This helps to find the actual ScID of an object
    # rather than just the ScID of the last imported scenario. Needed in stdResponseHandler()
    # and updateobject()
    scn['timestamp'] = update['lastmodified']
    scn['scid'] = update['ScID']
    scno = scn.objects

    for feature in collect(update['geometry_output']['geometry']):
        try:
            p = feature['properties']
            layer = None
            if 'layer' in p:
                layer = p['layer']
            if 'geomID' in p:
                ID = p['geomID']
                name = str(scn['scid']) + "-" + str(ID)
                if 'geometry' not in feature:
                    print(feature)
                else:
                    d = geojson_2_bl_geometry(feature['geometry'], name)
                    if name in scno:
                        #print("*")
                        o = scno[name]
                        o['timestamp'] = str(p['timestamp'])
                        #o.matrix_world = Matrix()
                        o.data = d
                        if layer is not None:
                            swaplayer(o, layer)
                    else:
                        #print("+")
                        o = bpy.data.objects.new(name, d)
                        o['geomID'] = ID
                        o['timestamp'] = str(p['timestamp'])
                        if layer is not None:
                            swaplayer(o, layer)
                        scno.link(o)
            elif 'delete_list' in p:
                for item in p['delete_list']:
                    if item in scno:
                        scno.unlink(item)

        except Exception as e:
            print(e)
            d = geojson_2_bl_geometry(feature['geometry'], "geom")
            o = bpy.data.objects.new("geom", d)
            scno.link(o)

def getGroup(name):
    if name in bpy.data.groups:
        return bpy.data.groups[name]
    else:
        return bpy.data.groups.new(name)

def swaplayer(o, layer):
    for group in o.users_group:
        group.objects.unlink(o)
    group = getGroup(layer)
    group.objects.link(o)


class BlenderResponseHandler(ResponseHandler):
    def handleProgress(self, message):
        pass

    def handleError(self, message):
        raise IOError(message.getHeader()['error'])

class stdResponseHandler(BlenderResponseHandler):
    def handleResult(self, message):
        global camera, camera_updated
        h = message.getHeader()
        if "scenario.camera.Update" in h['serviceName']:
            r = h["result"]
            lookAt = r["lookAt"]
            location = r["location"]
            cameraUp = r["cameraUp"]
            scale = r["scale"]
            camera = {'lookAt': {'x':lookAt["x"], 'y': lookAt["y"], 'z': lookAt["z"]},
                      'location': {'x': location["x"], 'y': location["y"], 'z': location["z"]},
                      'cameraUp': {'x': cameraUp["x"], 'y': cameraUp["y"], 'z': cameraUp["z"]},
                      'scale': scale}
            camera_updated = True
        elif "scenario.geojson.Get" == h["serviceName"]:
            import_scenario(h, bpy.context.scene)


class ScenarioUpdateHandler(BlenderResponseHandler):
    def __init__(self, obj):
        super().__init__()
        self.obj = obj

    def handleResult(self, message):
        r = message.getHeader()['result']
        if "newIDs" in r:
            newIDs = r['newIDs']
            self.obj.name = str(r['ScID']) + '-' + str(newIDs[0])
            self.obj['geomID'] = str(newIDs[0])
            self.obj['timestamp'] = str(int(time.time() * 1000))


def addobject(scn, obj):
    if 'scid' in scn:
        g = features2FeatureCollection(object2features(obj, scn, with_id=False))
        request = {"run": "scenario.geojson.Update", "ScID": scn['scid'], "geometry_input":{"format":"geojson", "geometry":g}}
        m = Message(request)

        rh = ScenarioUpdateHandler(obj)
        luci_client.send(m, rh)


def updatedobject(scn, obj):
    if 'scid' in scn:
        if scn.world is not None:
            g = features2FeatureCollection(object2features(obj, scn, with_id=True))
            request = {"run": "scenario.geojson.Update", "ScID": scn['scid'], "geometry_input": {"format":"geojson", "geometry":g}}
            m = Message(request)
            rh = ScenarioUpdateHandler(obj)
            luci_client.send(m, rh)


def deleteobjects(scn, ids):
    if 'scid' in scn:
        g = {"type": "FeatureCollection", "features": [{"type": "Feature", "properties": {"deleted_geomIDs": ids}}]}
        request = {"run": "scenario.geojson.Update", "ScID": scn['scid'], "geometry_input": {"format":"geojson", "geometry":g}}
        m = Message(request)
        rh = ScenarioUpdateHandler(None)
        luci_client.send(m, rh)


time_prev = time.time()
last_selected_ids = list()
updated_objects = list()
lookAt_prev = 0
matrix_prev = 0
# problem: too fast if object is dragged
# problem: too big scenarios fail to import
# problem: transforming raises index out of bound error sometimes

@persistent
def scene_update_post(scene):
    global time_curr, time_prev
    global last_selected_ids
    global camera, camera_updated, camera_subscribed

    # print(globals().mouse_pressed)

    active_object = scene.objects.active
    selected_ids = list()
    global updated_objects

    if scene.connected and scene.auto_sync:

        for object in scene.objects:
            if object.select and 'geomID' in object:
                selected_ids.append(object['geomID'])
                if object.is_updated or object.is_updated_data:
                    if object not in updated_objects:
                        # print("append object")
                        updated_objects.append(object)

        if active_object is not None and (active_object.is_updated or active_object.is_updated_data):
            if 'geomID' in active_object:
                if DEBUG:
                    print("update")

                updated = False

                time_curr = time.time()
                # print(time_curr-time_prev)
                if (time_curr - time_prev) > 0.1:
                    for updated_obj in updated_objects:
                        # updated_obj['timestamp'] = str(int(time.time() * 1000))
                        # print(updated_obj['timestamp'])
                        updatedobject(scene, updated_obj)
                        updated_objects = list()
                        updated = True
                if updated:
                    time_prev = time_curr
            else:
                if DEBUG:
                    print("add")
                addobject(scene, active_object)

        if active_object is None and len(last_selected_ids) != 0:
            if DEBUG:
                print("delete")
            deleteobjects(scene, last_selected_ids)

        last_selected_ids = selected_ids

        if scene.cameraControl == 'PUBLISH':
            for area in bpy.context.screen.areas:
                if area.type == 'VIEW_3D':
                    global lookAt_prev, matrix_prev
                    lookAt = area.spaces.active.region_3d.view_location
                    matrix = area.spaces.active.region_3d.view_matrix
                    # print("CURR", lookAt)
                    # print("PREV", lookAt_prev)
                    if lookAt_prev != lookAt or matrix_prev != matrix:
                        if scene.cameraID != 0:
                            cameraUp = matrix.inverted() * mathutils.Vector((0, 1, 0, 0))
                            location = matrix.inverted() * mathutils.Vector((0, 0, 1, 1))

                            request = Message({'run': 'scenario.camera.Update', 'cameraID': scene.cameraID,
                                               'cameraUp': {
                                                   'x': cameraUp[0],
                                                   'y': cameraUp[1],
                                                   'z': cameraUp[2]
                                               },
                                               'location': {
                                                   'x': location[0],
                                                   'y': location[1],
                                                   'z': location[2]
                                               },
                                               'lookAt': {
                                                   'x': lookAt[0],
                                                   'y': lookAt[1],
                                                   'z': lookAt[2]
                                               }})
                            luci_client.send(request, BlenderResponseHandler())

                    lookAt_prev = copy.deepcopy(lookAt)
                    matrix_prev = copy.deepcopy(matrix)
        elif scene.cameraControl == 'LISTEN':
            if not camera_subscribed:
                cameraID = scene.cameraID
                request = Message({'run': 'scenario.camera.SubscribeTo', 'cameraID': cameraID})
                luci_client.send(request, stdResponseHandler())
                camera_subscribed = True

            if camera_updated:
                for area in bpy.context.screen.areas:
                    if area.type == 'VIEW_3D':
                        view_matrix_new = mathutils.Matrix()

                        lookAt = mathutils.Vector((camera['lookAt']['x'], camera['lookAt']['y'], camera['lookAt']['z']))
                        cameraUp = mathutils.Vector((camera['cameraUp']['x'], camera['cameraUp']['y'], camera['cameraUp']['z']))
                        location = mathutils.Vector((camera['location']['x'], camera['location']['y'], camera['location']['z']))

                        forward = (lookAt.normalized() - location.normalized()).normalized()
                        #right = cameraUp.cross(forward).normalized()
                        cameraUp.normalize()
                        left = forward.cross(cameraUp)
                        u = left.normalized().cross(forward)
                        zDir = (location - lookAt).normalized()
                        xDir = cameraUp.cross(zDir).normalized()
                        yDir = zDir.cross(xDir)


                        # https://www.opengl.org/sdk/docs/man2/xhtml/gluLookAt.xml
                        view_matrix_new[0][0] = xDir[0]
                        view_matrix_new[1][0] = xDir[1]
                        view_matrix_new[2][0] = xDir[2]
                        view_matrix_new[3][0] = - location.dot(xDir)

                        view_matrix_new[0][1] = yDir[0]
                        view_matrix_new[1][1] = yDir[1]
                        view_matrix_new[2][1] = yDir[2]
                        view_matrix_new[3][1] = - location.dot(yDir)

                        view_matrix_new[0][2] = zDir[0]
                        view_matrix_new[1][2] = zDir[1]
                        view_matrix_new[2][2] = zDir[2]
                        view_matrix_new[3][2] = - location.dot(zDir)

                        view_matrix_new[0][3] = 0
                        view_matrix_new[1][3] = 0
                        view_matrix_new[2][3] = 0
                        view_matrix_new[3][3] = 1

                        view_matrix_new.transpose()
                        #
                        # view_matrix_new[0][0] = left[0]
                        # view_matrix_new[0][1] = left[1]
                        # view_matrix_new[0][2] = left[2]
                        # view_matrix_new[0][3] = 0
                        #
                        # view_matrix_new[1][0] = u[0]
                        # view_matrix_new[1][1] = u[1]
                        # view_matrix_new[1][2] = u[2]
                        # view_matrix_new[1][3] = 0
                        #
                        # view_matrix_new[2][0] = -forward[0]
                        # view_matrix_new[2][1] = -forward[1]
                        # view_matrix_new[2][2] = -forward[2]
                        # view_matrix_new[2][3] = 0
                        #
                        # view_matrix_new[3][3] = location[0]
                        # view_matrix_new[3][3] = location[1]
                        # view_matrix_new[3][3] = location[2]
                        # view_matrix_new[3][3] = 1

                        #print(area.spaces.active.region_3d.view_matrix.inverted())
                        #print(area.spaces.active.region_3d.view_matrix)
                        #print(view_matrix_new)
                        area.spaces.active.region_3d.view_matrix = view_matrix_new
                        #area.spaces.active.region_3d.view_matrix = view_matrix_new.inverted()

                        camera_updated = False


def last_selected_clear():
    global last_selected_ids
    last_selected_ids = list()

